<?php

define('K_PATH_MAIN', Mage::getBaseDir());
define('K_PATH_CACHE', K_PATH_MAIN . DS . 'var' . DS . 'cache'  . DS);

require_once(dirname(__FILE__) . '/tcpdf.php');

class Pdf extends TCPDF
{

    const COLUMN_HEIGHT = 6;
    const FONT_SIZE_CAPTION = 10;
    const FONT_SIZE_ITEMS = 6;
    const TYPE_HEADER = 'header';
    const TYPE_FOOTER = 'footer';
    
    const DEFAULT_TYPE = 'default';
    
    const LANG_EN = 'eng';
    const LANG_NL = 'nld';
    const LANG_DE = 'ger';    

    protected $_itemsHeaderData = false;
    protected $_mainHeaderData = '';
    protected $_currentLanguage = self::LANG_EN;
    protected $_resetMainHeaderOnNewPage = false;
    protected $_typeOfDocument = 'default';

    /**
     * 
     * @param string $orientation
     * @param string $unit
     * @param string $format
     * @param mixed $unicode
     * @param type $encoding
     * @param bool $diskcache
     * @param mixed $pdfa
     * @param string $lang Init language
     */
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false, $lang = self::LANG_EN)
    {
        $this->_currentLanguage = $lang;
        require_once(dirname(__FILE__) . "/config/lang/{$lang}.php");
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }
    
    public function getItemsHeaderData()
    {
        return $this->_itemsHeaderData;
    }

    /**
     * Set static header
     */
    public function Header()
    {
        if (!$this->_resetMainHeaderOnNewPage) {
            $html = $this->_prepareData(self::TYPE_HEADER);
            $this->writeHTML($html, true, false, true, false, '');
        }
    }

    /**
     * Set static footer
     */
    public function Footer()
    {
        // Position at 55 mm from bottom
        $this->SetY(-45);

        $html = $this->_prepareData(self::TYPE_FOOTER);

        $this->writeHTML($html, true, false, true, false, '');

        // Set font
        $this->SetFont('courier', 'I', 8);
        // Page number
        $this->Cell(0, 10, Mage::helper('page')->__('Page') . ' ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    /**
     * Get pdf as sting 
     * @return string
     */
    public function render()
    {
        $output = $this->output(null, 'S');
        $this->_destroy(true);
        return $output;
    }

    /**
     * Proccess template
     * @param string $typeOfTemplate
     * @return string
     */
    private function _prepareData($typeOfTemplate)
    {

        $templateFile = dirname(__FILE__) . DS . '../' . DS . 'locale' . DS .
                $this->_currentLanguage . DS . $typeOfTemplate . DS . $this->_typeOfDocument . '.phtml';

        if ($content = file_get_contents($templateFile)) {
            $filter = new Mage_Core_Model_Email_Template_Filter();
            $filter->setVariables(array('model' => $this->_mainHeaderData));
            return $filter->filter($content);
        }

        return false;
    }

    public function AddPage($orientation = '', $format = '', $keepmargins = false, $tocpage = false)
    {
        parent::AddPage($orientation, $format, $keepmargins, $tocpage);

        $this->_addTableHeaderToNewPage();
    }

    /**
     * 
     * @param bool | array $itemsHeaderData Name and width of itme caption column 
     * @param Ct_Pdf_Model_Abstract $model
     * @param bool $resetMainHeaderOnNewPage
     */
    public function addPageExtended($model = FALSE, $resetMainHeaderOnNewPage = FALSE)
    {
        /**
         * Check model object
         */
        if ($model instanceof Wm_Pdf_Model_Abstract) {
            $this->_mainHeaderData = $model;

            $this->_typeOfDocument =
                    is_null($model->getDocumentType()) ?
                    self::DEFAULT_TYPE : $model->getDocumentType();

            /**
             * check if set $itemsHeaderData
             */
            if (is_array($model->itemsHeaderData)) {
                $this->_itemsHeaderData = $model->itemsHeaderData;
            }
        } else {
            $this->_mainHeaderData = new Varien_Object ();
        }

        $this->AddPage();

        if ($this->_resetMainHeaderOnNewPage = $resetMainHeaderOnNewPage) {
            //set margins
            $this->SetMargins(PDF_MARGIN_LEFT, self::COLUMN_HEIGHT, PDF_MARGIN_RIGHT);
        }
    }

    /**
     * Add additive page top offset 
     * @param int $h
     * @param string $y
     * @param boolean $addpage
     * @return boolean
     */
    protected function checkPageBreak($h = 0, $y = '', $addpage = true)
    {
        if ($result = parent::checkPageBreak($h = 0, $y = '', $addpage = true)) {
            $this->y += self::COLUMN_HEIGHT;
            return $result;
        }
        return false;
    }

    /**
     * Add caption of table new 
     */
    private function _addTableHeaderToNewPage()
    {
        if (is_array($this->_itemsHeaderData)) {

            // set font
            $this->SetFont('dejavusans', '', self::FONT_SIZE_CAPTION);

            $this->SetTextColor(58, 76, 127);
            $this->SetFillColor(220, 220, 220);

            foreach ($this->_itemsHeaderData as $columnName => $columnWidth) {
                $this->Cell($columnWidth, self::COLUMN_HEIGHT, Mage::helper('sales')->__($columnName), 'LRTB', 0, 'C', true);
            }
            $this->Ln();

            $this->setFontSize(self::FONT_SIZE_ITEMS);
        }
    }

    /**
     * Rewrite catch errors
     * @param string $msg
     */
    public function Error($msg)
    {
        if (Mage::getIsDeveloperMode()) {
            parent::Error($msg);
        } else {
            Mage::log('TCPDF ERROR: ' . $msg, null, 'pdf.log');
        }
    }


}
