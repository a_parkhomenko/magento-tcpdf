<?php

class Wm_Pdf_Model_Shipment extends Wm_Pdf_Model_Abstract
{

    const TOP_OFFSET = 65;
    const BOTTOM_OFFSET = 30;

    public function getPdf($shipments = array())
    {
         /**
         * Max width 165
         */
         $this->itemsHeaderData = array(
            'Product name' => 50,
            'SKU' => 45,
            'Price' => 20,
            'Qty' => 30,
            'Tax' => 20,
            'Subtotal' => 20,
        );

        //Init invoice item rendering
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        if (current($shipments)->getStoreId()) {
            Mage::app()->getLocale()->emulate(current($shipments)->getStoreId());
            Mage::app()->setCurrentStore(current($shipments)->getStoreId());
        }
        
        $langCode = Mage::app()->getLocale()->getLocaleCode() == 'nl_NL' ? Pdf::LANG_NL : Pdf::LANG_EN;

        $this->_pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, false, $langCode);

        $this->_setDocumentInformation();

        // set default monospaced font
        $this->_pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $this->_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + self::TOP_OFFSET, PDF_MARGIN_RIGHT);
        /*
         * System get header margin value 
         * lowest than top_margin 
         * equal to header_margin
         * 
         * Header margin don't work without margin_top  !
         */
        $this->_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

        /**
         * Footer margin will ignore if set
         * autobreak
         */
//        $this->_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        $this->_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM + self::BOTTOM_OFFSET);

        //set image scale factor
        $this->_pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $this->getOrderAndAddressDetails(current($shipments));

        // add a page
        $this->_pdf->AddPageExtended($this);

        /**
         * Get all item from invoice 
         * and draw it
         */
        foreach ($shipments as $shipment) {

//            if ($shipment->getStoreId()) {
//                Mage::app()->getLocale()->emulate($shipment->getStoreId());
//                Mage::app()->setCurrentStore($shipment->getStoreId());
//            }

            $order = $shipment->getOrder();

            /* Add body */
            foreach ($shipment->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }

                $this->_drawItem($item, $order);
            }
        }

        $this->insertTotals($shipment);

        if ($shipment->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }

        return $this->_pdf;
    }

    protected function insertTotals($source)
    {
        /**
         * Set size options
         */
        $offset = 5;
        $totalCellWidth = 70;

        /**
         * Set default options for total
         */
        $this->_pdf->SetAutoPageBreak(FALSE);
        $this->_pdf->setCellMargins(0, 0, 0, 0);
        $this->_pdf->setCellPaddings(0, 0, 0, 0);

        $order = $source->getOrder();

        $totalParams = array(
            'subtotal' => Mage::helper('sales')->__('Subtotaal'),
            'base_subtotal' => Mage::helper('sales')->__('Totaal excl btw'),
            'base_tax_amount' => Mage::helper('sales')->__('BTW'),
            'base_subtotal_incl_tax' => Mage::helper('sales')->__('Totaal incl btw')
        );

        $marginRight = $this->_pdf->getPageWidth() - $totalCellWidth + $offset - PDF_MARGIN_RIGHT;

        $table =
                '<style>table {width: 100%; } ' .
                'td {border: 0,5px solid black; height: 20pt; } ' .
                'td.label {width: 50px;}' .
                '</style>';

        $table .= '<table>';

        foreach ($totalParams as $key => $label) {

            $paramName = 'get' . uc_words($key, '');
            $amountValue = $order->formatPriceTxt($order->{$paramName}());

            $table .=
                    "<tr><td class='label'><dl><dt> {$label}</dt></dl></td>" .
                    "<td class='amount'><dl><dt> {$amountValue}</dt></dl></td></tr>";
        }

        $table .= '</table>';

        $this->_pdf->writeHTMLCell($totalCellWidth, Pdf::COLUMN_HEIGHT, $marginRight, $this->_pdf->GetY(), $table, 1);
        return $this;
    }

}
