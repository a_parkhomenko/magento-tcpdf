<?php

class Wm_Pdf_Model_Items_Invoice_Grouped extends Wm_Pdf_Model_Items_Invoice_Default
{
    /**
     * Draw process
     */
    public function draw()
    {
        $type = $this->getItem()->getOrderItem()->getProduct()->getTypeId();
        $renderer = $this->getRenderedModel()->getRenderer($type);
        
        $renderer->setOrder($this->getOrder());
        $renderer->setItem($this->getItem());
        $renderer->setPdf($this->getPdf());

        $renderer->draw();
    }
}
