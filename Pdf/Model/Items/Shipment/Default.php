<?php

class Wm_Pdf_Model_Items_Shipment_Default extends Wm_Pdf_Model_Items_Abstract
{
    /**
     * Draw item line
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();       
        
        $extendedData = $item->getOrderItem()->getOrigData();
        
         /* Get product */
        /**
         * @todo check product type
         */
        $product  = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
        
        /**
         * Get info about table caption
         *
         */
        if (!$headerData = $pdf->getItemsHeaderData()) {
            throw new Exception ('Wm_Pdf - Something wrong with input data');
            return false;
        }
                
        $pdf->Cell($headerData['Product name'], Pdf::COLUMN_HEIGHT, substr($this->getName($item) , 0 , 25), 'LRTB', 0, 'C');
        
        $pdf->Cell($headerData['SKU'], Pdf::COLUMN_HEIGHT, substr($this->getSku($item) , 0, 25), 'LRTB', 0, 'C');        
        
        /**
         *  Name of attributes depends on store name
         */
        // custom options
//        $options = $this->getItemOptions();
//        
//        $color = is_null($options->getKleur()) ? $options->getColor() : $options->getKleur();
//        $size = is_null($options->getMaat()) ? $options->getSize() : $options->getMaat();
//        
//        $pdf->Cell($headerData['Color'], Pdf::COLUMN_HEIGHT, $color, 'LRTB', 0, 'C');
//        $pdf->Cell($headerData['Size'], Pdf::COLUMN_HEIGHT, $size, 'LRTB', 0, 'C');
        
        /*
         * Get price and subtotals
         */
        list($prices) = $this->getItemPricesForDisplay();  
        
        $pdf->Cell($headerData['Price'], Pdf::COLUMN_HEIGHT, $prices['price'], 'LRTB', 0, 'C');
        
        $pdf->Cell($headerData['Qty'], Pdf::COLUMN_HEIGHT, (int) $item->getQty(), 'LRTB', 0, 'C');
        
        $pdf->Cell($headerData['Tax'], Pdf::COLUMN_HEIGHT, $order->formatPriceTxt($extendedData['base_tax_amount']), 'LRTB', 0, 'C');      
        
        $pdf->Cell($headerData['Subtotal'], Pdf::COLUMN_HEIGHT, $order->formatPriceTxt($extendedData['base_row_total_incl_tax']) , 'LRTB', 0, 'C');  
        
//           /* get special attribute */
//        $adviesprijs = $product->getMsrp() ? $order->formatPriceTxt($product->getMsrp()) : '-';
//        
//        $pdf->Cell($headerData['MSRP'], Pdf::COLUMN_HEIGHT, $adviesprijs , 'LRTB', 0, 'C'); 
            
        
        $pdf->Ln();
    }
}
