<?php

require_once Mage::getModuleDir('', 'Wm_Pdf') . DS . 'lib' . DS . 'pdf.php';

abstract class Wm_Pdf_Model_Abstract extends Varien_Object
{

    /**
     * Item renderers with render type key
     *
     * model    => the model name
     * renderer => the renderer model
     *
     * @var array
     */
    protected $_renderers = array();

    /**
     * Predefined constants
     */

    const XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID = 'sales_pdf/invoice/put_order_id';
    const XML_PATH_SALES_PDF_SHIPMENT_PUT_ORDER_ID = 'sales_pdf/shipment/put_order_id';
    const XML_PATH_SALES_PDF_CREDITMEMO_PUT_ORDER_ID = 'sales_pdf/creditmemo/put_order_id';

    /**
     * Pdf object
     *
     * @var Pdf
     */
    protected $_pdf;

    /**
     * Default total model
     *
     * @var string
     */
    protected $_defaultTotalModel = 'sales/order_pdf_total_default';
    
    
    protected $_commonInfo = '';
    protected $_orderDetails = NULL;
    protected $_headerBillingAdress = NULL;
    protected $_headerShippingAdress = NULL;
    
    /**
     * Items caption with width of cells
     * @var array 
     */
    public $itemsHeaderData = array();
    
    /**
     * Document type
     */
    protected $_documentType = NULL;
    
    /**
     * Organization name
     * @var string
     */
    protected $_organization = 'Bonnewitzz';
   


    /**
     * Retrieve PDF
     *
     * @return Pdf
     */
    abstract public function getPdf();
    
    public function getOrderDetails()
    {
        return $this->_orderDetails;
    }
    
    public function getHeaderBillingAdress()
    {
        return $this->_headerBillingAdress;
    }
    
    public function getHeaderShippingAdress()
    {
        return $this->_headerShippingAdress;
    }
    
    public function getDocumentType()
    {
        return $this->_documentType;
    }

    /**
     * Insert document information in output document
     */
    protected function _setDocumentInformation()
    {
        $this->_pdf->SetCreator($this->_organization);
        $this->_pdf->SetAuthor($this->_organization);
        $this->_pdf->SetTitle($this->_organization);
        $this->_pdf->SetSubject($this->_organization);
        $this->_pdf->SetKeywords($this->_organization);
    }

    /**
     * Insert order to invoice pdf page
     *
     * @param Mage_Sales_Model_Order $obj
     */
    protected function getOrderAndAddressDetails ($obj, $invoice = NULL)
    {
        /**
         * Check obj type
         */
        if ($obj instanceof Mage_Sales_Model_Order) {
            $shipment = null;
            $order = $obj;            
        } elseif ($obj instanceof Mage_Sales_Model_Order_Shipment) {
            $shipment = $obj;
            $order = $shipment->getOrder();
            
            /**
             * Only for vadotex (need the same id of document invoice and package slip)
             * 
             */
            $invoice = $this->_convertShippingToInvoice($order);            
        }  
        
        switch (get_class($this)) {
            case 'Wm_Pdf_Model_Invoice':
                $this->_documentType = 'invoice';
                break;
            
            case 'Wm_Pdf_Model_Shipment':
                $this->_documentType = 'shipment';
                break;
            
            default:
                $this->_documentType = 'default';
                break;;
        }

        $orderId = $order->getEntityId();
        if ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            
           
            
            $orderBillingAddress = $order->getBillingAddress();
            
            $billingAddress = new Varien_Object ();            
            /**
             * Set one street
             */
            $street = current($orderBillingAddress->getStreet());
            
            $billingAddress->setData($orderBillingAddress->getData());
            
            $billingAddress->setStreet($street);
            $billingAddress->setCountryName(
                    Mage::getModel('directory/country')->load(
                                $billingAddress->getCountryId())->getName());
            
            /**
             * Set header factuur address
             */
            $this->_headerBillingAdress = $billingAddress;
            
            
            $this->_orderDetails = new Varien_Object ();
            
            if ($invoice instanceof Mage_Sales_Model_Order_Invoice) {
                $this->_orderDetails->setData('invoice_id', $invoice->getIncrementId());
            }
            
            $this->_orderDetails->setData('order_id', $order->getRealOrderId());
            $this->_orderDetails->setData('date_create', $order->getCreatedAtStoreDate());
            $this->_orderDetails->setData('customer_id', $billingAddress->getCustomerId());
        }      
        
        if (!$order->getIsVirtual()) {

            if ($orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);
                
                $shippingAddress = new Varien_Object ();
                
                $orderShippingAddress = $order->getShippingAddress();
                
                $shippingAddress->setData($orderShippingAddress->getData());                
                $shippingAddress->setCountryName(
                        Mage::getModel('directory/country')->load(
                                $orderShippingAddress->getCountryId())->getName());
                
                /**
                 * Set header factuur address
                 */
                $this->_headerShippingAdress = $shippingAddress;
            }
        }
        
    }
    
    /**
     * 
     * @param Mage_Sales_Model_Order $order
     * @return Mage_Sales_Model_Order_Invoice | null
     */
    private function _convertShippingToInvoice ($order)
    {
        /* Set invoice number to shipment */
        $invoices = $order->getInvoiceCollection()->getAllIds();
        $shipmentToInvoice = NULL;

        if (is_array($invoices) && current($invoices)){
            $shipmentToInvoice = Mage::getSingleton('sales/order_invoice')->load(current($invoices));
        }

        return $shipmentToInvoice;
    }


    /**
     * Sort totals list
     *
     * @param  array $a
     * @param  array $b
     * @return int
     */
    protected function _sortTotalsList($a, $b)
    {
        if (!isset($a['sort_order']) || !isset($b['sort_order'])) {
            return 0;
        }

        if ($a['sort_order'] == $b['sort_order']) {
            return 0;
        }

        return ($a['sort_order'] > $b['sort_order']) ? 1 : -1;
    }

    /**
     * Return total list
     *
     * @param  Mage_Sales_Model_Abstract $source
     * @return array
     */
    protected function _getTotalsList($source)
    {
        $totals = Mage::getConfig()->getNode('global/pdf/totals')->asArray();
        usort($totals, array($this, '_sortTotalsList'));
        $totalModels = array();
        foreach ($totals as $index => $totalInfo) {
            if (!empty($totalInfo['model'])) {
                $totalModel = Mage::getModel($totalInfo['model']);
                if ($totalModel instanceof Mage_Sales_Model_Order_Pdf_Total_Default) {
                    $totalInfo['model'] = $totalModel;
                } else {
                    Mage::throwException(
                            Mage::helper('sales')->__('PDF total model should extend Mage_Sales_Model_Order_Pdf_Total_Default')
                    );
                }
            } else {
                $totalModel = Mage::getModel($this->_defaultTotalModel);
            }

            $totalModel->setData($totalInfo);
            $totalModels[] = $totalModel;
        }

        return $totalModels;
    }

    /**
     * Insert totals to pdf page
     *
     * @param  Mage_Sales_Model_Abstract $source
     * @return Zend_Pdf_Page
     */
    protected function insertTotals($source)
    {
        /**
         * Set size options
         */
        $offset = 5;
        $totalCellWidth = 70;
        
        /**
         * Set default options for total
         */
        $this->_pdf->SetAutoPageBreak(FALSE);
        $this->_pdf->setCellMargins(0,0,0,0);
        $this->_pdf->setCellPaddings(0,0,0,0);

        $order = $source->getOrder();
        $totals = $this->_getTotalsList($source);

        $marginRight = $this->_pdf->getPageWidth() - $totalCellWidth + $offset - PDF_MARGIN_RIGHT;

        $table = 
                '<style>table {width: 100%; } ' .
                'td {border: 0,5px solid black; height: 20pt; } '.
                'td.label {width: 50px;}' .
                '</style>';        
        
        $table .= '<table>';
        
        foreach ($totals as $total) {
            $total->setOrder($order)
                    ->setSource($source);

            if ($total->canDisplay()) {
                
                foreach ($total->getTotalsForDisplay() as $totalData) {
                    $table .= 
                            "<tr><td style='width: 60%; text-align: left;'><dl><dt> {$totalData['label']}</dt></dl></td>" .
                            "<td class='amount'><dl><dt> {$totalData['amount']}</dt></dl></td></tr>";
                }
            }
        }
        
        $table .= '</table>';
        
        $this->_pdf->writeHTMLCell($totalCellWidth, Pdf::COLUMN_HEIGHT, $marginRight, $this->_pdf->GetY(), $table, 1);
        return $this;
    }

    /**
     * Parse item description
     *
     * @param  Varien_Object $item
     * @return array
     */
    protected function _parseItemDescription($item)
    {
        $matches = array();
        $description = $item->getDescription();
        if (preg_match_all('/<li.*?>(.*?)<\/li>/i', $description, $matches)) {
            return $matches[1];
        }

        return array($description);
    }

    /**
     * Before getPdf processing
     */
    protected function _beforeGetPdf()
    {
        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);
    }

    /**
     * After getPdf processing
     */
    protected function _afterGetPdf()
    {
        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(true);
    }

    /**
     * Format option value process
     *
     * @param  array|string $value
     * @param  Mage_Sales_Model_Order $order
     * @return string
     */
    protected function _formatOptionValue($value, $order)
    {
        $resultValue = '';
        if (is_array($value)) {
            if (isset($value['qty'])) {
                $resultValue .= sprintf('%d', $value['qty']) . ' x ';
            }

            $resultValue .= $value['title'];

            if (isset($value['price'])) {
                $resultValue .= " " . $order->formatPrice($value['price']);
            }
            return $resultValue;
        } else {
            return $value;
        }
    }

    /**
     * Initialize renderer process
     *
     * @param string $type
     */
    protected function _initRenderer($type)
    {
        $node = Mage::getConfig()->getNode('global/pdf/' . $type);
        foreach ($node->children() as $renderer) {
            $this->_renderers[$renderer->getName()] = array(
                'model' => (string) $renderer,
                'renderer' => null
            );
        }
    }

    /**
     * Retrieve renderer model
     *
     * @param  string $type
     * @throws Mage_Core_Exception
     * @return Mage_Sales_Model_Order_Pdf_Items_Abstract
     */
    protected function _getRenderer($type)
    {
        if (!isset($this->_renderers[$type])) {
            $type = 'default';
        }

//        if (!isset($this->_renderers[$type])) {
//            Mage::throwException(Mage::helper('sales')->__('Invalid renderer model'));
//        }


        if (is_null($this->_renderers[$type]['renderer'])) {
            $this->_renderers[$type]['renderer'] = Mage::getSingleton($this->_renderers[$type]['model']);
        }

        return $this->_renderers[$type]['renderer'];
    }

    /**
     * Public method of protected @see _getRenderer()
     *
     * Retrieve renderer model
     *
     * @param  string $type
     * @return Mage_Sales_Model_Order_Pdf_Items_Abstract
     */
    public function getRenderer($type)
    {
        return $this->_getRenderer($type);
    }

    /**
     * Draw Item process
     *
     * @param  Varien_Object $item
     * @param  Mage_Sales_Model_Order $order
     * @return Pdf
     */
    protected function _drawItem(Varien_Object $item, Mage_Sales_Model_Order $order)
    {
        $type = $item->getOrderItem()->getProductType();
        $renderer = $this->_getRenderer($type);
        $renderer->setOrder($order);
        $renderer->setItem($item);
        $renderer->setPdf($this->_getPdf());

        $renderer->setRenderedModel($this);

        $renderer->draw();

        return $renderer->getPage();
    }

    /**
     * Set PDF object
     *
     * @param  Pdf $pdf
     * @return Mage_Sales_Model_Order_Pdf_Abstract
     */
    protected function _setPdf(Pdf $pdf)
    {
        $this->_pdf = $pdf;
        return $this;
    }

    /**
     * Retrieve PDF object
     *
     * @throws Mage_Core_Exception
     * @return Pdf
     */
    protected function _getPdf()
    {
        if (!$this->_pdf instanceof Pdf) {
            Mage::throwException(Mage::helper('sales')->__('Please define PDF object before using.'));
        }

        return $this->_pdf;
    }

}