<?php

class Wm_Pdf_Model_Creditmemo extends Wm_Pdf_Model_Abstract
{

    const TOP_OFFSET = 65;
    const BOTTOM_OFFSET = 30;
    
    public function getPdf($creditmemos = array())
    {
        /**
         * Max width 165
         */
         $this->itemsHeaderData = array(
            'Product name' => 50,
            'SKU' => 45,
            'Price' => 20,
            'Qty' => 30,
            'Tax' => 20,
            'Subtotal' => 20,
        );
         
        //Init invoice item rendering
        $this->_beforeGetPdf();
        $this->_initRenderer('creditmemo');
        
        if (current($creditmemos)->getStoreId()) {
            Mage::app()->getLocale()->emulate(current($creditmemos)->getStoreId());
            Mage::app()->setCurrentStore(current($creditmemos)->getStoreId());
        }
        
        $langCode = Mage::app()->getLocale()->getLocaleCode() == 'nl_NL' ? Pdf::LANG_NL : Pdf::LANG_EN;
        
        $this->_pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, false, $langCode);

        $this->_setDocumentInformation();

        // set default monospaced font
        $this->_pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $this->_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + self::TOP_OFFSET, PDF_MARGIN_RIGHT);
        /* 
         * System get header margin value 
         * lowest than top_margin 
         * equal to header_margin
         * 
         * Header margin don't work without margin_top  !
         */
        $this->_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        
        /**
         * Footer margin will ignore if set
         * autobreak
         */
//        $this->_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $this->_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM + self::BOTTOM_OFFSET);

        //set image scale factor
        $this->_pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                
        $firstOrder = current($creditmemos)->getOrder();
        
        $this->getOrderAndAddressDetails($firstOrder, current($creditmemos));

        // add a page
        $this->_pdf->AddPageExtended($this, TRUE);

       /**
         * Get all item from invoice 
         * and draw it
         */
        foreach ($creditmemos as $creditmemo) {

//            if ($invoice->getStoreId()) {
//                Mage::app()->getLocale()->emulate($invoice->getStoreId());
//                Mage::app()->setCurrentStore($invoice->getStoreId());
//            }

            $order = $creditmemo->getOrder();

            /* Add body */
            foreach ($creditmemo->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }

                $this->_drawItem($item, $order);
            }
        }
        
        $this->insertTotals($creditmemo);

        if ($creditmemo->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }

        return $this->_pdf;
    }

}
